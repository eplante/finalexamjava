public class Recursion {
	public static int recursiveCount(String[] words, int n) {
		int firstNonEmptyIndex = getFirstNonEmptyIndex(words);
		
		if (firstNonEmptyIndex == -1) return 0;
	
		boolean wordValid = isWordValid(words[firstNonEmptyIndex], firstNonEmptyIndex, n);
		
		words[firstNonEmptyIndex] = "";
		
		if(wordValid) return 1 + recursiveCount(words, n);
		else return 0 + recursiveCount(words, n);
	}
	
	private static int getFirstNonEmptyIndex(String[] words) {
		for(int i = 0; i < words.length; i++) {
			if(!words[i].equals("")) return i;
		}
		
		return -1;
	}
	
	private static boolean isWordValid(String word, int index, int n) {
		return word.contains("w") 
		    && index % 2 == 1 
		    && index >= n;
	}
	
	public static void main(String[] args) {
		String[] d = {"test", "word", "word2"};
    	
    	System.out.println(recursiveCount(d,  0));
	}
}