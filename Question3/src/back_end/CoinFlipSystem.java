package back_end;

//Singleton class containing all necessary values for the game.
public class CoinFlipSystem {
	private static CoinFlipSystem instance = null;
	
	private int money;
	private int currentBetAmount;
	private int side;
	
	//Singleton so hiding the constructor
	private CoinFlipSystem() {
		this.money = 100;
		this.currentBetAmount = 0;
		this.side = 0;
	}
	
	public static CoinFlipSystem getInstance() {
		if(instance == null) instance = new CoinFlipSystem();
		
		return instance;
	}
	
	//Get the current money amount
	public int getMoney() {
		return money;
	}

	//Set the current money amount
	public void setMoney(int money) {
		this.money = money;
	}

	//Get the current bet amount
	public int getCurrentBetAmount() {
		return currentBetAmount;
	}

	//Get the current bet amount
	public void setCurrentBetAmount(int currentBetAmount) {
		this.currentBetAmount = currentBetAmount;
	}
	
	//Get the side the user is currently betting on
	public int getSide() {
		return side;
	}

	//Get the side the user is currently betting on
	public void setSide(int side) {
		this.side = side;
	}
}
