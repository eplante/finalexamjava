package front_end;

import back_end.CoinFlipSystem;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The entry point of the UI application
 */
public class CoinFlipUI extends Application {
    public void start(Stage stage) {
        Group root = new Group();

        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.BLACK);

        root.getChildren().add(createUI());

        stage.setTitle("Coin Flip");
        stage.setScene(scene);
        stage.show();
    }

    //Creates all the UI
    private VBox createUI() {
    	VBox overall = new VBox();
    	HBox moneyBox = new HBox();
    	
    	Text feedback = new Text();
    	feedback.setFill(Color.RED);
    	
    	//The bet textField and current money amount text
    	TextField betAmount = new TextField();
    	betAmount.setPromptText("Enter a bet amount");
    	Text currentMoney = new Text("You currently have " + CoinFlipSystem.getInstance().getMoney() + "$");
    	currentMoney.setFill(Color.WHITE);
    	
    	HBox headsTails = createHeadsTailsButtons(betAmount, feedback);
    	
    	Button playBtn = new Button("Play");
    	playBtn.setOnAction(new CoinFlipAction(currentMoney, feedback));
    	
    	moneyBox.getChildren().addAll(betAmount, currentMoney);
    	overall.getChildren().addAll(moneyBox, headsTails, playBtn, feedback);
    	
    	return overall;
    }
    
    //Creates the buttons that let the user choose between heads and tails
    private HBox createHeadsTailsButtons(TextField betAmount, Text feedback) {
    	HBox headsTails = new HBox();
    	Button heads = new Button("Heads");
    	Button tails = new Button("Tails");
    	heads.setOnAction(new SetBetAction(betAmount, 0, feedback));
    	tails.setOnAction(new SetBetAction(betAmount, 1, feedback));
    	headsTails.getChildren().addAll(heads, tails);
    	
    	return headsTails;
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
