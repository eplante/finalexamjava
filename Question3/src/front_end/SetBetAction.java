package front_end;

import back_end.CoinFlipSystem;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class SetBetAction implements EventHandler<ActionEvent> {
	private TextField bet;
	private Text feedback;
	private int side;
	
	public SetBetAction(TextField bet, int side, Text feedback)
	{
		this.bet = bet;
		this.feedback = feedback;
		this.side = side;
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		CoinFlipSystem coinFlip = CoinFlipSystem.getInstance();
		
		try {
			int betAmount = Integer.parseInt(bet.getText());
			
			//If the bet is valid, set the required values
			if(betAmount <= coinFlip.getMoney()) {
				coinFlip.setCurrentBetAmount(betAmount);
				coinFlip.setSide(side);
				feedback.setText("");
			}
			else { //if the bet is higher than the user's money
				feedback.setText("Bet value too high");
			}
		} catch(NumberFormatException e) { //if the bet is not a number
			feedback.setText("Bet not a number");
		}
	}
}
