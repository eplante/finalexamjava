package front_end;

import java.util.Random;

import back_end.CoinFlipSystem;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;

public class CoinFlipAction implements EventHandler<ActionEvent> {
	private Text currentMoney;
	private Random rand;
	private Text feedback;
	
	public CoinFlipAction(Text currentMoney, Text feedback) {
		this.currentMoney = currentMoney;
		this.rand = new Random();
		this.feedback = feedback;
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		CoinFlipSystem coinFlip = CoinFlipSystem.getInstance();
		
		//If the randomizer chose the same value as the user, add money and set the text
		if((rand.nextBoolean() ? 1 : 0) == coinFlip.getSide()) {
			coinFlip.setMoney(coinFlip.getMoney() + coinFlip.getCurrentBetAmount());
			feedback.setText("You win! You got " + coinFlip.getCurrentBetAmount() + "$");
		}
		else {
			coinFlip.setMoney(coinFlip.getMoney() - coinFlip.getCurrentBetAmount());
			feedback.setText("You lose! You lost " + coinFlip.getCurrentBetAmount() + "$");
		}
		
		//Set the current amount of money text
		currentMoney.setText("You currently have " + coinFlip.getMoney() + "$");
	}
}
